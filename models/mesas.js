const {Schema, model}  = require('mongoose');

const MesasSchema = Schema({
    nombre:{
        type: String,
        uppercase: true, 
        trim: true,
        required: true
    }
});

MesasSchema.method('toJSON', function() {
    const {__v, ...object} = this.toObject();
    return object;
})

module.exports = model( 'Mesas', MesasSchema );