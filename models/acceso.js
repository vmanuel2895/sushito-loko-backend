const {Schema, model}  = require('mongoose');

const AccesosSchema = Schema({
    idUsuario:{
        type: Schema.Types.ObjectId,
        required: true
    },
    idModulo:{
        type: Schema.Types.ObjectId,
        required: true
    }
});

AccesosSchema.method('toJSON', function() {
    const {__v, ...object} = this.toObject();
    return object;
})

module.exports = model( 'Accesos', AccesosSchema );