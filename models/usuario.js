const {Schema, model}  = require('mongoose');

const UsuarioSchema = Schema({
    nombre:{
        type: String,
        uppercase: true, 
        trim: true,
        required: true
    },
    /* email:{
        type: String,
        required: true,
        unique: true
    }, */
    password:{
        type: String,
        required: true  
    },
    departamento:{
        type:String,
        required: true
    },
    designacion:{
        type:String,
        required:true
    },
    telefono:{
        type:String,
        required:true
    },
    fechaIngreso:{
        type:Date,
        required:true
    },
    status:{
        type:String,
        default:'ACTIVO',
    },
    img:{
        type: String,
    },
    role:{
        type: String,
        required: true,
        default: 'USER_ROLE'
    }
});

UsuarioSchema.method('toJSON', function() {
    const {__v, _id,password, ...object} = this.toObject();  
    object.uid = _id;
    return object;
})

module.exports = model( 'Usuario', UsuarioSchema );