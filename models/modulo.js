const {Schema, model}  = require('mongoose');

const ModulosSchema = Schema({
    name:{
        type: String,
        uppercase: true, 
        trim: true,
        required: true
    },
    route:{
        type: String,
        required: true
    },
    img:{
        type:String
    }
});

ModulosSchema.method('toJSON', function() {
    const {__v, ...object} = this.toObject();
    return object;
})

module.exports = model( 'Modulos', ModulosSchema );