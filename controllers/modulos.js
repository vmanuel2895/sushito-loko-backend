const { response } = require('express');
const Modulo = require('../models/modulo');

const crearModulo = async(req, res = response)=>{
    try {
        const modulo = new Modulo(req.body);
        //guardar modulos
        await modulo.save(); 
        res.json({
            ok:true,
            modulo
        });
    } catch (error) {
        res.status(500).json({
            ok:false,
            msg:'Error inesperado... revisar logs'
        })
    }
}

module.exports = {
    crearModulo,
}