const { response } = require("express");
const bcrypt = require('bcryptjs');
const Usuario = require("../models/usuario");
const { generarJWT } = require("../helpers/jwt");

const login = async(req,res=response)=>{

    const { nombre, password } = req.body
    try {
        //verificar email
        const usuarioDB = await Usuario.findOne({nombre});
        if(!usuarioDB){
            return res.status(404).json({
                ok:false,
                msg:'El nombre ingresado no se encuentra registrado'
            });
        }
        //Verificar contraseña
        const validPassword = bcrypt.compareSync( password, usuarioDB.password );
        if(!validPassword){
           return res.status(400).json({
                ok:false,
                msg:'La contraseña que se ingreso no es valida'
           }) 
        }
        //Generar JWT
        const token = await generarJWT( usuarioDB.id );
        res.json({
            ok:true,
            token,
            usuarioDB
        })
    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok:false,
            msg:'Hable con el administrador'
        })
    }
}

const renewToken = async(req, res = response) => {
    const uid = req.uid;
    // Generar el TOKEN - JWT
    const token = await generarJWT( uid );
    const usuarioDB = await Usuario.findById(uid);
    res.json({
        ok: true,
        token,
        usuarioDB
    });
}

module.exports={
    login,
    renewToken
}