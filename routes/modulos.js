/* 
    Ruta: /api/modulos
*/
const { Router } = require('express');
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar-campos');

const router = Router();
const { crearModulo } = require('../controllers/modulos');

router.post('/', 
    [
        check('name', 'El nombre es obligatorio').not().isEmpty(),
        check('route', 'La ruta es obligatoria').not().isEmpty(),
        validarCampos
    ],
    crearModulo
);

module.exports = router;